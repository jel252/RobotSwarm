% 2015-08-05
% Detect nearby agents
% By Jeng-Hau Lin

% output: a list of agents' numbers within communication range. The current
% agent i is excluded.
function N = detect(i)
N = [];
global agents;
global Rd;
global t_current;

for ind = 1:length(agents)
    if(ind ~= agents(i).num) % Exclude the curretn agent i
       dist = norm(agents(i).qhis{t_current,i} - agents(ind).qhis{t_current,ind});
       if(dist <= Rd)
            N = [N ind];
       end
    end
end

end