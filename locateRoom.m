% 2015-08-05
% Locate room number with position
% By Jeng-Hau Lin

function rmj = locateRoom(pos)
global rooms;

for ind = 1 : length(rooms)
    if((pos(1) >= rooms{ind}(1)) && (pos(2) >= rooms{ind}(2)) &&...
            (pos(1)<rooms{ind}(1)+rooms{ind}(3)) &&...
            (pos(2)<rooms{ind}(2)+rooms{ind}(4)))
        rmj = ind;
        break;
    end
end

end