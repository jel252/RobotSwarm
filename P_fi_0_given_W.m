% 2015-08-07
% P(fi=0|W) = pf_bar * prod_j_in_W P(fi=0|j)
% This is a local version of  P(fi=0|H)
% By Jeng-Hau Lin

% input: the number of the current agent i
% input: time t 
% output: P_fi0_gv_W = P(fi=0|W)
function P_fi0_gv_W = P_fi_0_given_W(i,t,W,dx ,dy)
global pf;
global Ehj;
global agents;
pf_bar = 1-pf;
P_fi0_gv_W = pf_bar;

if(isempty(W))
    P_fi0_gv_W = 1;
    return
end

for ind = 1 : length(W)
    P_fi0_gv_W = P_fi0_gv_W * (1-alpha_qi_Ehj((agents(i).qhis{t,i} + [dx dy]),Ehj(W(ind))));
end

end