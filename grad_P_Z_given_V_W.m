% 2015-08-03
% P(Z|X,H)
% By Jeng-Hau Lin

% input: i is the number of the current agent
% input: Z is the array of zi stored in the current agent i
% input: V is the array of target room numbers of the current coalition
% input: W is the array of hazard romm numbers of the current coalition

function grad_P_Z_gv_V_W = grad_P_Z_given_V_W(i, Z, V, W)
global agents;
global t_current;

P_Z_gv_V_W = zeros(length(agents),1);

% Calculate a list of all zi in P(Z|V,W)
for ind = 1 : length(agents)
    
    if(Z(ind) == 0)
        P_zi_gv_fi_V = [P_zi_0_given_fi_0_V(i, t_current, V, 0 ,0), 1]; %[f=0, f=1]
    else
        P_zi_gv_fi_V = [(1 - P_zi_0_given_fi_0_V(i, t_current, V, 0, 0)), 0]; %[f=0, f=1]
    end
%     P_zi_gv_fi_V
    
    P_fi0_gv_W = P_fi_0_given_W(i, t_current, W, 0, 0);
    P_fi1_gv_W = 1-  P_fi_0_given_W(i, t_current, W, 0, 0);
    P_fi_gv_W = [P_fi0_gv_W, P_fi1_gv_W].'; %[f=0 f=1]
% 	 P_fi_gv_W
    P_Z_gv_V_W(ind) = P_zi_gv_fi_V * P_fi_gv_W;
end

grad_P_Z_gv_V_W_arr = ones(length(agents),2);
for ind = 1 : length(agents)
%     grad_P_zi_gv_V_W = grad_P_zi_given_V_W(i, Z(ind), V, W);
grad_P_zi_gv_V_W = grad_P_zi_given_V_W2(i, Z(ind), V, W);

    for j = 1: length(agents)
        if(j == ind)
            grad_P_Z_gv_V_W_arr(ind,:) = grad_P_Z_gv_V_W_arr(ind) .* grad_P_zi_gv_V_W ;
        else
            grad_P_Z_gv_V_W_arr(ind,:) = grad_P_Z_gv_V_W_arr(ind) * P_Z_gv_V_W(j);
        end
    end
end

grad_P_Z_gv_V_W = sum(grad_P_Z_gv_V_W_arr,1);

end