% 2015-08-02
% calculate mj for the integral of target or hazard probability
% By Jeng-Hau Lin

function mj_arr = cal_mj(x,R)

R_sq = R^2;
mj_arr = [];
global Q

for ind = 1:numel(Q)
   if ( norm(Q{ind}-x) <= R_sq)
        mj_arr = [mj_arr Q(ind)];
   end
end

end