% 2015-08-12
% grad of P(zi|V,W)
% By Jeng-Hau Lin

function grad_P_zi_0_gv_V_W = grad_P_zi_given_V_W2(i, zi, V, W)
global agents;
global Esj;
global Ehj;
global t_current;
% gradP = [1 0];

% % Assume zi == 0
% % Calculate [P(zi=0|fi=0,V) P(zi=0|fi=1,V)]
% P_zi_gv_V_W0 = P_zi_0_given_fi_0_V(i, t_current, V, 0 , 0);
% 
% P_zi_gv_V_Wdx = P_zi_0_given_fi_0_V(i, t_current, V, 1 , 0);
% 
% 
% P_zi_gv_V_Wdy = P_zi_0_given_fi_0_V(i, t_current, V, 0 , 1);
% 
% % gradP1f0 = [(P_zi_gv_V_W0 - P_zi_gv_V_Wdx), (P_zi_gv_V_Wdy - P_zi_gv_V_W0)];
% 
% 
% P_fi0_gv_W0 = P_fi_0_given_W(i, t_current, W, 0 , 0);
% P_fi0_gv_Wdx = P_fi_0_given_W(i, t_current, W, 0.5 , 0);
% P_fi0_gv_Wdy = P_fi_0_given_W(i, t_current, W, 0 , 0.5);
% 
% P_fi1_gv_W0 = 1 - P_fi0_gv_W0;
% P_fi1_gv_Wdx = 1 - P_fi0_gv_Wdx;
% P_fi1_gv_Wdy = 1 - P_fi0_gv_Wdy;
% 
% P0  = P_zi_gv_V_W0  * P_fi0_gv_W0  + P_fi1_gv_W0;
% Pdx = P_zi_gv_V_Wdx * P_fi0_gv_Wdx + P_fi1_gv_Wdx;
% Pdy = P_zi_gv_V_Wdy * P_fi0_gv_Wdy + P_fi1_gv_Wdy;
% 
% gradP = [Pdx, Pdy] - P0;
% 
% if (zi == 0)
%     gradP = - gradP;
% else
%     gradP = gradP;
% end
% % gradP2 = []
% % if(zi == 0)
%     

P_fi_0_gv_W = P_fi_0_given_W(i ,t_current, W, 0, 0);

P_zi_0_gv_fi_0_V = P_zi_0_given_fi_0_V(i,t_current,V,0,0);
P_fi_0_gv_W = P_fi_0_given_W(i,t_current,W, 0 ,0);
P_zi_0_gv_V_W = P_zi_0_gv_fi_0_V * P_fi_0_gv_W;

sumV = [0 0];
for indV = 1 : length(V)
    sumV =  sumV + 1/(1-mu_qi_Esj(agents(i).qhis{t_current,i},Esj(V(indV))))*...
        [(mu_qi_Esj(agents(i).qhis{t_current,i}+[-1 0],Esj(V(indV)))-...
        mu_qi_Esj(agents(i).qhis{t_current,i},Esj(V(indV)))),...
        (mu_qi_Esj(agents(i).qhis{t_current,i}+[0 -1],Esj(V(indV)))-...
        mu_qi_Esj(agents(i).qhis{t_current,i},Esj(V(indV))))];
end

sumW = [0 0];
for indW = 1 : length(W)
    sumW =  sumW + 1/(1-alpha_qi_Ehj(agents(i).qhis{t_current,i},Ehj(W(indW))))*...
        [(alpha_qi_Ehj(agents(i).qhis{t_current,i}+[-1 0],Ehj(W(indW)))-...
        alpha_qi_Ehj(agents(i).qhis{t_current,i},Ehj(W(indW)))),...
        (alpha_qi_Ehj(agents(i).qhis{t_current,i}+[-1 0],Ehj(W(indW)))-...
        alpha_qi_Ehj(agents(i).qhis{t_current,i},Ehj(W(indW))))];
end

gradP = -P_fi_0_gv_W*P_zi_0_gv_V_W*sumV + P_fi_0_gv_W*(1-P_zi_0_gv_V_W)*sumW;

if(zi == 0)
    grad_P_zi_0_gv_V_W = gradP;
else
    grad_P_zi_0_gv_V_W = -gradP;
end
end