% 2015-08-06
% Bayesian estimation for phi
% By Jeng-Hau Lin

% input: the number of the current agent i
% input: t might be the time of past or current
% input: Coa_arr is the coalition to which i belongs
% input: Vcs the Vc of this coalition
function estimatePhi(i, t, Coa_arr, Vcs)
global agents;
global t_current;

Z = agents(i).zhis{t};
F = agents(i).fhis{t};


% Calculate P(Z|F,V) for every V in the coalition
P_Z_gv_F_V = ones(length(Vcs(:,1)),1);
% lengthCoa_arr = length(Coa_arr)
for indCoa = 1 : length(Coa_arr)
    
    % Calculate P(zi|fi,X)
    P_zi_gv_fi_V = zeros(length(Z),1);
    for ind = 1 : length(Z)
        if((Z(ind) == 0) && (F(ind) ==0))
            P_zi_gv_fi_V(ind) = P_zi_0_given_fi_0_V(i,t,Vcs{indCoa}, 0,0);
        elseif((Z(ind) == 1) && (F(ind) ==0))
            P_zi_gv_fi_V(ind) = 1 - P_zi_0_given_fi_0_V(i,t,Vcs{indCoa}, 0,0);
        elseif((Z(ind) == 0) && (F(ind) ==1))
            P_zi_gv_fi_V(ind) = 1;
        elseif((Z(ind) == 1) && (F(ind) ==1))
            P_zi_gv_fi_V(ind) = 0;
        end
%         'P_Z_gv_F_V(indCoa,1) = ' 
%         P_Z_gv_F_V(indCoa,1)
%         'P_zi_gv_fi_V(ind) = '
%         P_zi_gv_fi_V(ind)
        P_Z_gv_F_V(indCoa,1) = P_Z_gv_F_V(indCoa,1) * P_zi_gv_fi_V(ind);
    end
end


% numerator is a vector
numerators = zeros(1, length(Coa_arr));
for indCoa = 1 : length(Coa_arr)
    aphi = agents(i).phihis{t}(Coa_arr(indCoa));
    numerators(indCoa) = P_Z_gv_F_V(indCoa) * agents(i).phihis{t}(Coa_arr(indCoa));
end

denominator = sum(numerators)+1e-9;

newPhi_Coa = numerators/ denominator;

% Store all phi into the history table
for indCoa = 1: length(Coa_arr)
%     If a new row of estimation is going to be generated
    if(length(agents(i).phihis) == t_current)
        agents(i).phihis{t+1,1} = zeros(1, length(agents));
    end
    
    agents(i).phihis{t+1,1}(Coa_arr(indCoa)) = newPhi_Coa(indCoa);
end

end