% 2015-08-06
% Bayesian estimation for psi
% By Jeng-Hau Lin

% input: the number of the current agent i
% input: t might be the time of past or current
% input: Coa_arr is the coalition to which i belongs
% input: Wcs the Wc of this coalition
function estimatePsi(i, t, Coa_arr, Wcs)
global agents;
global t_current;

F = agents(i).fhis{t};


% Calculate P(F|W) for every W in the coalition
P_F_gv_W = ones(length(Wcs(:,1)),1);
% lengthCoa_arr = length(Coa_arr)
for indCoa = 1 : length(Coa_arr)
    
    % Calculate P(fi|W)
    P_fi_gv_W = zeros(length(F),1);
    for ind = 1 : length(F)
        if(F(ind) ==0)
            P_fi_gv_W(ind) = P_fi_0_given_W(i,t,Wcs{indCoa}, 0, 0);
        elseif(F(ind) ==1)
            P_fi_gv_W(ind) = 1 - P_fi_0_given_W(i,t,Wcs{indCoa}, 0, 0);
        end
        P_F_gv_W(indCoa,1) = P_F_gv_W(indCoa,1) * P_fi_gv_W(ind);
    end
end


% numerator is a vector
numerators = zeros(1, length(Coa_arr));
for indCoa = 1 : length(Coa_arr)
numerators(indCoa) = P_F_gv_W(indCoa) * agents(i).psihis{t}(Coa_arr(indCoa));
end

denominator = sum(numerators);

newPsi_Coa = numerators/ denominator;


for indCoa = 1: length(Coa_arr)
    %     If a new row of estimation is going to be generated
    if(length(agents(i).psihis) == t_current)
        agents(i).psihis{t+1,1} = zeros(1, length(agents));
    end
    
    agents(i).psihis{t+1,1}(Coa_arr(indCoa)) = newPsi_Coa(indCoa);
end

end