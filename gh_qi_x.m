% 2015-08-02
% Failure model
% By Jeng-Hau Lin

function gh = gh_qi_x(qi, x)
pfn = 0.1;
sigma = 1.5;
R = 2;

pfn_bar = 1-pfn;
sigma_sq = sigma^2;
norm_qi_x = norm(qi-x);

if( norm_qi_x <= R)
    gh = pfn_bar * exp(-(norm_qi_x.^2)/sigma_sq);
else
    gh = 0;
end


end