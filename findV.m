% 2015-08-06
% Collect a list target numbers of which room that both covered by agent's footprint 
% and occupied by a target.
% By Jeng-Hau Lin

% input: a number of the current agent: i
% input: time t
function V = findV(i,t)
global Esj;
global Rc;
global rooms;
global agents;

% Vi = Fi union X

% Build a list for current footprints
% Find a list of room center position
l = rooms{1}(3);
roomCenter = cell(1,length(rooms));
for ind = 1 : length(rooms)
    roomCenter{1,ind} = [(rooms{ind}(1) + l/2) (rooms{ind}(2) + l/2)];
end

% Build a list for the rooms covered by footprint
FRmj = [];
for ind = 1:length(roomCenter)
    distx = norm(agents(i).qhis{t,i}(1)-roomCenter{ind}(1));
    disty = norm(agents(i).qhis{t,i}(2)-roomCenter{ind}(2));

    if(distx<(Rc+l/2) &&(disty <(Rc+l/2)))
        FRmj = [FRmj ind];
    end
end

% Wrong!
% Collect a list of room that both covered by agent's footprint 
% and occupied by a target.
% Wrong!

% Collect a list of target numbers of which the room is also covered by the
% agent's footprint.
V = []; 
for(indFRmj = 1 : length(FRmj))
    for(indEsj = 1: length(Esj))
        if(FRmj(indFRmj) == Esj(indEsj).rmj)
            V = [V indEsj];
        end
    end
end

end