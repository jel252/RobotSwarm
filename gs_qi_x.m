% 2015-08-02
% Sensor model
% By Jeng-Hau Lin

function gs = gs_qi_x(qi, x)
pfn = 0.05;
sigma = 2;
R = 6;

pfn_bar = 1-pfn;
sigma_sq = sigma^2;
norm_qi_x = norm(qi-x);

if( norm_qi_x <= R)
    gs = pfn_bar * exp(-(norm_qi_x.^2)/sigma_sq);
else
    gs = 0;
end


end