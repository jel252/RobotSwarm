% 2015-07-11
% Decentralization
% By Jeng-Hau Lin
clc; clear all; close all;

numAgents = 4;
numTargets = 3;
numHazards = 1;
global Rc;
Rc = 3;  % Footprint radius
global Rd;
Rd = 3; % Communicate distance

% Room size
x = [0:1:15];
y = [0:1:15];
global rooms;
l = 5;
% room = {[0 0 5 6],[0 6 2 3], [2 6 7 3], [5 0 4 6]};
% rooms = {[0 0 20 20], [20 0 20 20], [0 20 20 20], [20 20 20 20]};
rooms = {[0 0 l l], [l 0 l l], [2*l 0 l l], [0 l l l], [l l l l], [2*l l l l], [0 2*l l l], [l 2*l l l], [2*l 2*l l l]};

phi_1 = numTargets/length(rooms);
psi_1 = numHazards/length(rooms);

global Q;
Q = repmat({[0 0]},length(x), length(y));
for indx = 1:length(x)
   for indy = 1:length(y)
       Q{indx,indy} = [x(indx), y(indy)];
   end
end
% Q = x.' * y;

global Esj;
EsjNum  = {1,        2          ,3};
EsjPos  = {[4,8],    [7, 12],   [12.5 4.5]};
R_s = 1;
EsjMj   = {cal_mj(EsjPos{1},R_s), cal_mj(EsjPos{2},R_s), cal_mj(EsjPos{3},R_s)};
EsRmj   = {locateRoom(EsjPos{1}),       locateRoom(EsjPos{2}),       locateRoom(EsjPos{3})};
Esj = struct(...
        'num', EsjNum,...
        'pos', EsjPos,...
        'mjs', EsjMj,...
        'rmj', EsRmj);

global Ehj;
EhjNum  = {1};
EhjPos = {[12, 9]};
R_h = 2;
EhjMj   = {cal_mj(EhjPos{1},R_h)};
EhRmj   = {locateRoom(EhjPos{1})};
Ehj = struct(...
        'num', EhjNum,...
        'pos', EhjPos,...
        'mjs', EhjMj,...
        'rmj', EhRmj);

global agents;
agentName = {'WallE', 'Eva', 'Choppi', 'Sunny'};
agentNum = {1, 2, 3, 4};
% agentPos = {[6 3], [6 6], [10 2], [10 12]};
% agentPos = {[12 7], [6 6], [10 2], [10 12]};
agentPos = {[3 3], [6 6], [10 2], [12 12]};
% agentPos = {[0 0], [0 0], [0 0], [0 0]};
agentClr = {[0 0.3 0], [0 0.5 0], [0 0.7 0], [0 0.9 0]};
agentRmj = {locateRoom(agentPos{1}),       locateRoom(agentPos{2}),       locateRoom(agentPos{3}),       locateRoom(agentPos{4})};
agents = struct(...
        'name', agentName,...
        'num', agentNum,...
        'clr', agentClr,...
        'tauj',ones(1, numAgents),...           % a list for the last sync times
        'qhis',{agentPos, agentPos, agentPos, agentPos},...      % history of this agent's position
        'zhis',{{zeros(1,numAgents)}, {zeros(1,numAgents)},{zeros(1,numAgents)}, {zeros(1,numAgents)}},...  % history z of of all agents, which had ever exchanged information
        'fhis',{{zeros(1,numAgents)}, {zeros(1,numAgents)},{zeros(1,numAgents)}, {zeros(1,numAgents)}},...  % history z of of all agents, which had ever exchanged information
        'phihis', {{phi_1*ones(1,numAgents)}, {phi_1*ones(1,numAgents)},{phi_1*ones(1,numAgents)}, {phi_1*ones(1,numAgents)}},...
        'psihis', {{psi_1*ones(1,numAgents)}, {psi_1*ones(1,numAgents)},{psi_1*ones(1,numAgents)}, {psi_1*ones(1,numAgents)}}); 
%         'pos', agentPos,...
%         'phi', phi_1,...
%         'psi', psi_1,...
%         'zi', 0,...
%         'fi', 0,...
%         'rmj', agentRmj,...
%     agents = agents(1);
global pf;
global pfp; 
pf = 0.001;
pfp = 0.001;

global Coa;
global t_current;


figure(1)
    randomCounter = 0;

for t=1:40
    t_current = t
    
    % Detect targets and hazards     
    for i = 1 : length(agents)
        if(t == 1)
            break;
        end
        
        agents(i).fhis{t} = agents(i).fhis{t-1};
        agents(i).zhis{t} = agents(i).zhis{t-1};
%         Detect hazard first
        agents(i).fhis{t}(i) = 0;
        for indEhj = 1 : length(Ehj)
            dist = norm(Ehj(indEhj).pos - agents(i).qhis{t,i});
            if(dist <= Rc)
                agents(i).fhis{t}(i) = 1;
            end
        end
%         If fi != 0 -> zi could be 1
        agents(i).zhis{t}(i) = 0;
        for indEsj = 1 : length(Esj)
            dist = norm(Esj(indEsj).pos - agents(i).qhis{t,i});
            if((dist <= Rc) && (agents(i).fhis{t}(i) == 0))
                agents(i).zhis{t}(i) = 1;
            end
        end
        

    end
    
    
    % Draw the room
    for i = 1: length(rooms)
        rectangle('Position',rooms{i},...
                  'EdgeColor',[0.5,0.5,0.5],...
                  'LineStyle',':')
        axis equal
    end
    % Draw all targets and hazards
    for i = 1: length(Esj)
        rectangle('Position',[(Esj(i).pos(1)-0.5), (Esj(i).pos(2)-0.5), 1, 1],...
                  'Curvature',1,...
                  'EdgeColor','b',...
                  'FaceColor','b')
    end
    for i = 1: length(Ehj)
        rectangle('Position',[(Ehj(i).pos(1)-0.5), (Ehj(i).pos(2)-0.5), 1, 1],...
                  'Curvature',[1 1],...
                  'EdgeColor',[1,0.9,0.9],...
                  'FaceColor',[1,0.5,0.5])
    end
    
    % and robots
    for i = 1:length(agents)
        
        rectangle('Position',[(agents(i).qhis{t,i}(1)-0.5), (agents(i).qhis{t,i}(2)-0.5), 1, 1],...
                  'EdgeColor','k',...
                  'FaceColor',agents(i).clr)
        rectangle('Position',[(agents(i).qhis{t,i}(1)-Rc), (agents(i).qhis{t,i}(2)-Rc), 2*Rc, 2*Rc],...
                  'Curvature',[1 1],...
                  'EdgeColor','k')%,...
    end
    axis([0 15 0 15]);


% Update agents: communication and update bayesian estimation and calculate
% next step.


% phase 1: update each other within communication range Rd

for i = 1: numAgents

    %   Discover robots in communication range -> Ni
    
    N = detect(i);
    for j = 1:length(N)
        k = N(j);
%         'before if'
        % Lookup time of last communication, tauj
        if(agents(i).tauj(k) < t)
%             'if is true'
            % Exchange q^(tauj:t), z^(tauj:t), f^(t)
            % Send qi^(tauj:t), zi^(tauj:t), fi^(t)
            % Receive qj^(tauj:t), zj^(tauj:t), fj^(t)
            for s = agents(i).tauj(k)+1:t
                agents(k).qhis{s,i} = agents(i).qhis{s,i};
                agents(i).qhis{s,k} = agents(k).qhis{s,k};
            
                agents(k).zhis{s}(i) = agents(i).zhis{s}(i);
                agents(i).zhis{s}(k) = agents(k).zhis{s}(k);
            end
            
            agents(k).fhis{t}(i) = agents(i).fhis{t}(i);
            agents(i).fhis{t}(k) = agents(k).fhis{t}(k);
            
            % Update the sync time
            agents(i).tauj(k) = t;
            agents(k).tauj(i) = t;
        end
    end
end


% Phase 2: Coalition Identification for Robot i
% The minimum of Coa is the current agent it self.
QueueOuter = 1: numAgents;
QueueInner = [];
Coa = {};
counter_Coa = 1;

while (~isempty(QueueOuter))
   ind = QueueOuter(1);
   QueueOuter(1) = [];
   QueueInner = [QueueInner ind];
   
%    k = 1;
%    Qpre = QueueInner;
   Qcur = QueueInner;
   
   while(~isempty(QueueInner))
%        pop QueueInner
       ind = QueueInner(1);
       QueueInner(1) = [];
       curr_pos = agents(ind).qhis{t_current,ind};
       
       for j = 1 : length(QueueOuter)
            dist = norm(curr_pos - agents(QueueOuter(j)).qhis{t_current,QueueOuter(j)});
            if(dist <= 2*Rc)
%               pick QueueOuter(j) and push QueneInner and Qcur
                QueueInner = [QueueInner QueueOuter(j)];
                Qcur = [Qcur QueueOuter(j)];
%                 
                QueueOuter(j) = 0;
            end
       end
       QueueOuter(find(QueueOuter == 0)) = [];

   end
   
   Coa{counter_Coa,1} = Qcur;
   counter_Coa= counter_Coa+1;
end


% Calculate the next step of each agent
for i = 1: numAgents
    
    
    % Build Vcs (a list of all V, targets covered by the footprints) and
    % Wcs (a list of all W, hazards covered by the footprints)
    % find the coalition of current agent i    
    Coa_arr = [];
    for indCoa = 1 : length(Coa)
        find_Result = find(Coa{indCoa} == i);
        if(~isempty(find_Result))
            Coa_arr = Coa{indCoa}';
            break;
        end
    end
    
    % then build the corresponding Vcs     
    % Watch out Vcs could be empty
    Vcs = cell(length(Coa_arr),1);
    % length(Coa_arr)
    for indCoa = 1 : length(Coa_arr)
        Vcs{indCoa,1} = findV(Coa_arr(indCoa),t);
    end
    
    % then build the corresponding Vcs
    % Watch out Wcs could be empty
    Wcs = cell(length(Coa_arr),1);
    % length(Coa_arr)
    for ind = 1 : length(Coa_arr)
        Wcs{ind,1} = findW(Coa_arr(ind),t);
    end

    
    
    
    % Update Baysian estimation
    % This will always update from t==1 to t==t_current+1 based on the facts in agent i.     
    % updateAgent(i);
    for s = 1 : t
        estimatePhi(i, t, Coa_arr, Vcs);
        estimatePsi(i, t, Coa_arr, Wcs);
    end
    
    
    
    
    % Calculate the gradient of the mutual information I(Vc,Zc;q)
    'Begin update step'
    grad_I_V_Z = [0 0];
    for indCoa = 1 : length(Coa_arr)
        for indVcs1 = 1 : length(Vcs)
            for indWcs1 = 1:length(Wcs)
                % Calculate the gradient of P(Z|V,W) times phi and psi and the log
%                 grad_Z_gv_V_W = [0 0];
%                 dx = 1;
%                 dy = 1;
%                 P_Z_gv_V_W0 = P_Z_given_V_W(i, agents(Coa_arr(indCoa)).zhis{t}, Vcs{indVcs1}, Wcs{indWcs1}, 0,0);
%                 P_Z_gv_V_Wdx = P_Z_given_V_W(i, agents(Coa_arr(indCoa)).zhis{t}, Vcs{indVcs1}, Wcs{indWcs1}, dx,0);
%                 P_Z_gv_V_Wdy = P_Z_given_V_W(i, agents(Coa_arr(indCoa)).zhis{t}, Vcs{indVcs1}, Wcs{indWcs1}, 0, dy);
%                 grad_Z_gv_V_W(1,1) =  ( P_Z_gv_V_Wdx - P_Z_gv_V_W0)/dx;
%                 grad_Z_gv_V_W(1,2) =  ( P_Z_gv_V_Wdy - P_Z_gv_V_W0)/dy;
                grad_Z_gv_V_W = grad_P_Z_given_V_W(i, agents(Coa_arr(indCoa)).zhis{t}, Vcs{indVcs1}, Wcs{indWcs1});

                % the log part
                % Calculate numerator
%                 i
                logNumer = 0;
                for indWcs3 = 1 : length(Wcs)
                    P_Z_gv_V_W = P_Z_given_V_W(i, agents(Coa_arr(indCoa)).zhis{t}, Vcs{indVcs1}, Wcs{indWcs3}, 0,0);
                    logNumer = logNumer + P_Z_gv_V_W * agents(i).psihis{t+1}(i);
                end
                
%                 numerator
                
                logDemon = 0;
                % Calculate denominator
                for indVcs2 = 1 : length(Vcs)
                    for indWcs2 = 1 : length(Wcs)
                        P_Z_gv_V_W = P_Z_given_V_W(i, agents(Coa_arr(indCoa)).zhis{t}, Vcs{indVcs2}, Wcs{indWcs2}, 0,0);
                        logDemon = logDemon + P_Z_gv_V_W * agents(i).psihis{t+1}(Coa_arr(indWcs2)) * agents(i).phihis{t+1}(Coa_arr(indVcs2));
                    end
                end
%                 denominator
                epsilon = 0.0001;
%                 logPart = min(1e9,log(logNumer / logDemon) + epsilon);
                logPart = log(logNumer / logDemon) + epsilon;

                psi_cur = agents(i).psihis{t+1}(Coa_arr(indWcs1));
                phi_cur = agents(i).phihis{t+1}(Coa_arr(indVcs1));
                grad_I_V_Z  = grad_I_V_Z + grad_Z_gv_V_W * psi_cur * phi_cur * logPart;
            end
        end
    end
    
    
    % Calculate the next step
    for ind = 1 : length(agents)
        agents(i).qhis{t+1,ind} = agents(i).qhis{t,ind};
    end
    k = 0.3;
    epsilon2 = 1e-9;
    randomStep = [];
    nextStep = agents(i).qhis{t+1,i} + k * grad_I_V_Z / (norm(grad_I_V_Z) + epsilon2);
    while((sum(grad_I_V_Z) == 0) || ((nextStep(1) < x(1)) || (nextStep(1) > x(end)) ||...
               (nextStep(2) < y(1)) || (nextStep(2) > y(end))))
        rng shuffle;
        grad_I_V_Z = randi([-1 1],1,2);

        randomStep = agents(i).qhis{t+1,i} + 10* k * grad_I_V_Z / (norm(grad_I_V_Z) + epsilon2);
        nextStep = randomStep;
    end
    if(~isempty(randomStep))
        randomCounter = randomCounter +1;
    end
    agents(i).qhis{t+1,i} = nextStep;
    

end



    axis tight;
    frame = getframe(1);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    if t == 1;
        imwrite(imind,cm,'./output/RS01.gif','gif', 'Loopcount',inf);
    else
        imwrite(imind,cm,'./output/RS01.gif','gif','WriteMode','append');
    end
    hold off;
    close;
end