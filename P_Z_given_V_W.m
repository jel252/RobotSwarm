% 2015-08-03
% P(Z|X,H)
% By Jeng-Hau Lin

% input: i is the number of the current agent
% input: Z is the array of zi stored in the current agent i
% input: V is the array of target room numbers of the current coalition
% input: W is the array of hazard romm numbers of the current coalition

function P_Z_gv_V_W = P_Z_given_V_W(i, Z, V, W,dx ,dy)
global agents;
global t_current;

P_Z_gv_V_W = 1;

for ind = 1 : length(agents)
    
%     if(Z(ind) == 0 && fi == 0)
%         P_zi_gv_fi_X = P_zi_0_given_fi_0_X_H(agents(ind).pos);
%     elseif(Z(ind) == 1 && fi == 0)
%         P_zi_gv_fi_X = 1 - P_zi_0_given_fi_0_X_H(agents(ind).pos);
%     elseif(Z(ind) == 0 && fi == 1)
%         P_zi_gv_fi_X = 1;
%     else
%         P_zi_gv_fi_X = 0;
%     end
    
    if(Z(ind) == 0)
        P_zi_gv_fi_V = [P_zi_0_given_fi_0_V(i, t_current, V,-dx ,-dy), 1]; %[f=0, f=1]
    else
        P_zi_gv_fi_V = [(1 - P_zi_0_given_fi_0_V(i, t_current, V,dx ,dy)), 0]; %[f=0, f=1]
    end
%     P_zi_gv_fi_V
    
    P_fi0_gv_W = P_fi_0_given_W(i, t_current, W,-dx ,-dy);
    P_fi1_gv_W = 1-  P_fi_0_given_W(i, t_current, W, -dx ,-dy);
    P_fi_gv_W = [P_fi0_gv_W, P_fi1_gv_W].'; %[f=0 f=1]
% 	 P_fi_gv_W
    P_Z_gv_V_W = P_Z_gv_V_W * (P_zi_gv_fi_V * P_fi_gv_W);
end


end