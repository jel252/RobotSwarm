% 2015-08-06
% P(zi=0|fi=0,V) = pfp_bar *Prod_j_inV P(zi=0|fi=0,j) 
% This is a local version of  P(zi=0|fi=0,X,H)
% By Jeng-Hau Lin

% input: the number of the current agent i
% input: time t 
% output: P_zi0_gv_fi0_V = P(zi=0|fi=0,V)
function P_zi0_gv_fi0_V = P_zi_0_given_fi_0_V(i,t,V,dx,dy)
global pfp;
global Esj;
global agents;
pfp_bar = 1-pfp;
P_zi0_gv_fi0_V = pfp_bar;

if(isempty(V))
    P_zi0_gv_fi0_V = 1;
    return
end
    
for ind = 1 : length(V)
    pos = agents(i).qhis{t,i} + [dx dy];
%     V(ind)
    mu = mu_qi_Esj(pos,Esj(V(ind)));
    P_zi0_gv_fi0_V = P_zi0_gv_fi0_V * (1-mu);
end

end