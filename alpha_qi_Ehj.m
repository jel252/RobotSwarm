% 2015-08-02
% probability of failure due to hazard in cell Ehj
% By Jeng-Hau Lin

function alpha = alpha_qi_Ehj(qi, Ehj)
alpha = 0;
num_mj = length(Ehj.mjs);
for ind = 1:num_mj
    qh = gh_qi_x(qi,Ehj.mjs{ind});
    alpha = alpha + qh;
end
alpha = alpha/num_mj;
end