% 2015-08-02
% probability of detection of Esj
% By Jeng-Hau Lin

function mu = mu_qi_Esj(qi, Esj)
mu = 0;
num_mj = length(Esj.mjs);
for ind = 1:num_mj
    gs = gs_qi_x(qi,Esj.mjs{ind});
    mu = mu + gs;
end
mu = mu/num_mj;
end